PNG_MASK = ".png"
__author__ = 'dan'

import Image
import glob
import os

if __name__ == "__main__":
    import sys
    if len(sys.argv) < 3 :
        print ("you must provide an images dir path for script")
        sys.exit()

    print(sys.argv)
    script_path = os.path.normpath(sys.argv[1])
    columns     = int(sys.argv[2])

    print(script_path)


    x, y, height, width = 0, 0, 0, 0

    count = 0

    file_listing = os.listdir(script_path)
    file_listing.sort()

    for image_path in file_listing :
        if PNG_MASK in image_path:
            count += 1
            im = Image.open(script_path + os.path.sep + image_path)
            if (width < im.size[0]) or (height< im.size[1]) :
                width, height = im.size

    column_iterator = 0

    result = Image.new('RGBA', (width * columns, height * count / columns));

    for image_path in file_listing :
        if PNG_MASK in image_path:
            im = Image.open(script_path + os.path.sep + image_path)
            result.paste(im, (x, y))
            column_iterator += 1
            if (column_iterator < columns) :
                x += width
            else :
                column_iterator = 0
                x  = 0
                y += height

    x, y = 0, 0
    width /= 2
    height/= 2
    column_iterator = 0

    result_mini = Image.new('RGBA', (width * columns, height * count / columns));

    for image_path in file_listing :
        if PNG_MASK in image_path:
            im = Image.open(script_path + os.path.sep + image_path)
            im = im.resize((im.size[0] / 2, im.size[1] /2), Image.ANTIALIAS)
            result_mini.paste(im, (x, y))
            column_iterator += 1
            if (column_iterator < columns) :
                x += width
            else :
                column_iterator = 0
                x  = 0
                y += height

    result_path = script_path + os.path.sep + "result"

    if not os.path.exists(result_path):
        os.makedirs(result_path)

    result.save(result_path + os.path.sep + "result_image.png")
    result_mini.save(result_path + os.path.sep + "result_image_mini.png")

    print("image was created successfully. result is at path: " + result_path)